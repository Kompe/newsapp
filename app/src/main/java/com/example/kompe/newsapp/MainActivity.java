package com.example.kompe.newsapp;


//API KEY: 2a4418cc-f2c7-48dc-aee5-fa15575c4f32
//Query: http://content.guardianapis.com/search?section=games&show-fields=byline%2CtrailText&api-key=2a4418cc-f2c7-48dc-aee5-fa15575c4f32

import android.app.LoaderManager;

import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;


import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<NewsStory>> {

    public static final String LOG_TAG = MainActivity.class.getSimpleName();
    NewsStoryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArrayList<NewsStory> stories = new ArrayList<>();
        adapter = new NewsStoryAdapter(this, stories);


        ListView list = (ListView) findViewById(R.id.list);
        list.setAdapter(adapter);
        TextView emptyStateTextView = findViewById(R.id.emptystateview);
        list.setEmptyView(emptyStateTextView);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NewsStory story = stories.get(position);
                Intent websiteIntent = new Intent(Intent.ACTION_VIEW);
                websiteIntent.setData(Uri.parse(story.getUrl().toString()));
                startActivity(websiteIntent);
            }
        });

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            getLoaderManager().initLoader(0, null, this).forceLoad();
        } else {
            emptyStateTextView.setText(R.string.no_internet_connection);
        }


        SwipeRefreshLayout mSwipeRefreshLayout = findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.i(LOG_TAG, "called For Refresh");

                getLoaderManager().initLoader(0, null, MainActivity.this).reset();
                getLoaderManager().initLoader(0, null, MainActivity.this).forceLoad();
            }
        });
    }

    @Override
    public Loader<ArrayList<NewsStory>> onCreateLoader(int id, Bundle args) {
        return new NewsStoryLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<NewsStory>> loader, ArrayList<NewsStory> data) {
        adapter.clear();
        TextView emptyStateTextView = findViewById(R.id.emptystateview);

        ListView list = (ListView) findViewById(R.id.list);

        list.setAdapter(adapter);

        if (data != null && !data.isEmpty()) {
            Log.i(LOG_TAG, "DATA was not EMPTY.");
            adapter.addAll(data);
        }

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        emptyStateTextView.setText(R.string.nonews);
        if (networkInfo == null || !networkInfo.isConnected()) {
            emptyStateTextView.setText(R.string.no_internet_connection);
        }
        ((SwipeRefreshLayout) findViewById(R.id.swiperefresh)).setRefreshing(false);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<NewsStory>> loader) {
        adapter.clear();
    }


}
