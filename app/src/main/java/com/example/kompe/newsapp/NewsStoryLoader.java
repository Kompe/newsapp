package com.example.kompe.newsapp;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class NewsStoryLoader extends AsyncTaskLoader<ArrayList<NewsStory>> {

    public static final String LOG_TAG = NewsStoryLoader.class.getSimpleName();

    public NewsStoryLoader(Context context) {
        super(context);
    }

    @Override
    public ArrayList<NewsStory> loadInBackground() {
        ArrayList<NewsStory> stories = new ArrayList<>();
        String response = "";
        try {
            response = makeRequest();
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error getting the data ", e);
        }
        if (response != null) {
            stories = jsonToList(response);
        }
        return stories;
    }

    private String makeRequest() throws IOException {
        String response = "";
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        URL url = new URL("http://content.guardianapis.com/search?section=games&show-fields=byline%2CtrailText&api-key=2a4418cc-f2c7-48dc-aee5-fa15575c4f32");
        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.connect();

            if (connection.getResponseCode() == 200) {
                inputStream = connection.getInputStream();
                response = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Connection Error: " + connection.getResponseCode());
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem getting the data: ", e);
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return response;
    }

    private String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

    private ArrayList<NewsStory> jsonToList(String jsonString) {
        JSONArray resultlist = new JSONArray();
        ArrayList<NewsStory> stories = new ArrayList<>();
        try {
            JSONObject root = new JSONObject(jsonString);
            resultlist = root.getJSONObject("response").getJSONArray("results");
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Could not parse JSON String", e);
            return null;
        }
        for (int i = 0; i < resultlist.length(); i++) {
            JSONObject result = null;
            String headline = null;
            String section = null;
            String url = null;
            String date = null;
            String author;
            String trailText;
            try {
                result = (JSONObject) resultlist.get(i);
                headline = result.getString("webTitle");
                section = result.getString("sectionName");
                url = result.getString("webUrl");
                date = result.getString("webPublicationDate");

            } catch (JSONException e) {
                Log.e(LOG_TAG, "Could not parse JSON String", e);
                return null;
            }
            try {
                JSONObject fields = (JSONObject) result.getJSONObject("fields");
                author = fields.getString("byline");
                trailText = fields.getString("trailText");
            } catch (JSONException e) {
                Log.e(LOG_TAG, "Error retrieving Author or trailText, setting both to empty", e);
                author = "";
                trailText = "";
            }
            if (author != null && headline != null && date != null && trailText != null && url != null && section != null) {
                stories.add(new NewsStory(author, headline, date, trailText, url, section));
            }
        }
        Log.i(LOG_TAG, "RETURNED LIST SIZE: " + stories.size());
        return stories;
    }

}