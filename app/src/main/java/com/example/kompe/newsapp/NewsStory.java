package com.example.kompe.newsapp;


import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Locale;

public class NewsStory {

    private String author;
    private String headline;
    private Date date;
    private String trailText;
    private URL url;
    private String section;

    public NewsStory(String author, String headline, String date, String trailText, String url, String section){
        this.author=author;
        this.headline=headline;
        if(trailText != null) {
            if (trailText.length() > 200) {
                this.trailText = trailText.substring(0, 196) + "...";
            } else this.trailText = trailText;
        }
        else {
            this.trailText = null;
        }
        this.section = section;
        try{
            this.url = new URL(url);

            //yyyy-MM-ddThh:mm:ssZ
            int year = Integer.valueOf(date.substring(0,4))-1900;
            int month = Integer.valueOf(date.substring(5,7))-1;
            int day = Integer.valueOf(date.substring(8,10));
            System.out.println(year + " " + month + " " + day);

            this.date = new Date(year, month, day);

        }
        catch(MalformedURLException e){
            e.printStackTrace();
        }

    }

    public NewsStory(String headline, String date, String url, String section){
        this(null, headline, date, null, url, section);
    }

    public String getAuthor() {
        if(author==null){
            return "";
        }
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public Date getDate() {
        return date;
    }

    public String getFormattedDate(){
        SimpleDateFormat format = new SimpleDateFormat("LLL dd, yyyy", Locale.US);
        return format.format(date);
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTrailText() {
        if(trailText==null){
            return "";
        }
        return trailText;
    }

    public void setTrailText(String trailText) {
        this.trailText = trailText;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }
}
