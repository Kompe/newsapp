package com.example.kompe.newsapp;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class NewsStoryAdapter extends ArrayAdapter<NewsStory> {

    public NewsStoryAdapter(Context context, ArrayList<NewsStory> stories) {
        super(context, 0, stories);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }

        NewsStory current = (NewsStory) getItem(position);

        TextView view = (TextView) listItemView.findViewById(R.id.author_view);
        view.setText(current.getAuthor());
        view = (TextView) listItemView.findViewById(R.id.title_view);
        view.setText(current.getHeadline());
        view = (TextView) listItemView.findViewById(R.id.trail_text);
        view.setText(current.getTrailText());
        view = (TextView) listItemView.findViewById(R.id.date);
        view.setText(current.getFormattedDate());
        view = (TextView) listItemView.findViewById(R.id.section);
        view.setText(current.getSection());

        return listItemView;
    }

}
